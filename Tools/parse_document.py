#!/usr/bin/env python

"""
tw2html.py - A document analysis tool for the TechWriter Python package.

Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This file is part of the TechWriter Python package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import cmdsyntax
import struct, sys


class HTMLWriter:

    with_bytes = False
    
    def __init__(self, hex_digits):
    
        self.hex_digits = hex_digits
    
    def html(self, text):
    
        return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
    
    def write(self, data, output_file, start, finish, digits):
    
        if isinstance(data, Note):
            self.write_note(data, output_file)
        elif isinstance(data, Comment):
            self.write_comment(data, output_file)
        elif isinstance(data, HTMLReference):
            self.write_reference(data, output_file)
        elif isinstance(data, HTMLReferences):
            self.write_references(data, output_file)
        elif isinstance(data, Target):
            self.write_target(data, output_file)
        elif isinstance(data, Equation):
            self.write_equation(data, output_file)
        elif isinstance(data, Reference):
            obj = data.dereference()
            self.write_reference(HTMLReference(repr(obj), ("%%0%ix" % self.hex_digits) % obj.location), output_file)
        else:
            self.write_data_block(data, output_file, start, finish, self.with_bytes, digits)
    
    def write_line(self, data, offset, output_file, number, format, finish, with_bytes = False):
    
        hex_offset = format % offset
        
        bytes = data[:min(number, finish - offset)]
        if not bytes:
            return finish
    
        codes = []
        code = ""
        chars = []
        words = []
        word = ""
        word_styles = ["word-even", "word-odd"]
        for byte in bytes:
            if with_bytes:
                code = code + "%02x" % ord(byte) + "&nbsp;"
            word = "%02x" % ord(byte) + word
            if len(word) == 8:
                if with_bytes:
                    codes.append('<span class="' + word_styles[0] + '">' + code + '</span>')
                    code = ""
                words.append('<span class="' + word_styles[0] + '">' + word + '</span>')
                word_styles.insert(0, word_styles.pop())
                word = ""
            if 32 <= ord(byte) <= 126:
                chars.append(byte)
            else:
                chars.append('<span class="unprintable">?</span>')
        
        if word:
            word = "&nbsp;"*(8 - len(word)) + word
            words.append('<span class="' + word_styles[0] + '">' + word + '</span>')
            if with_bytes and code:
                codes.append('<span class="' + word_styles[0] + '">' + code + '</span>')
            word_styles.insert(0, word_styles.pop())
        
        if len(bytes) < number:
            remaining = ((number >> 2) - len(words))
            while remaining > 0:
                words += ['<span class="' + word_styles[0] + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>']
                word_styles.insert(0, word_styles.pop())
                remaining -= 1
            if with_bytes:
                codes += (number - len(bytes)) * ["&nbsp;&nbsp;"]
            chars += (number - len(bytes)) * ["&nbsp;"]
        
        if with_bytes:
            output_file.write('<span class="offset">' + hex_offset + '</span>' + ":&nbsp;"
                            + "&nbsp;".join(words) + "&nbsp;:&nbsp;"
                            + "".join(chars) + "&nbsp;:&nbsp;" + "".join(codes) + "\n")
        else:
            output_file.write('<span class="offset">' + hex_offset + '</span>' + ":&nbsp;"
                            + "&nbsp;".join(words) + "&nbsp;:&nbsp;"
                            + "".join(chars) + "\n")
        
        return offset + len(bytes)
    
    def write_lines(self, data, output_file, number, start, finish, with_bytes, digits):
    
        format = "%%0%dx" % digits
        offset = start
        
        while True:
            new_offset = self.write_line(data, offset, output_file, number, format,
                                         finish, with_bytes)
            data = data[new_offset-offset:]
            if new_offset >= finish:
                break
            offset = new_offset
    
    def write_header(self, path, output_file):
    
        output_file.write(
            "<html>\n"
            "<head>\n"
            "  <title> Hex dump of " + self.html(path) + "</title>\n"
            '  <link rel="stylesheet" type="text/css" href="styles.css" />\n'
            "</head>\n"
            "<body>\n"
            "<h1>" + self.html(path) + "</h1>\n"
            )
    
    def write_footer(self, output_file):
    
        output_file.write(
            "</body>\n"
            "</html>\n"
            )
    
    def write_data_block(self, data, output_file, start, finish, with_bytes, digits):
    
        output_file.write("<pre>\n")
        self.write_lines(data, output_file, 16, start, finish, with_bytes, digits)
        output_file.write("</pre>\n")
    
    def write_note(self, note, output_file):
    
        output_file.write("<h%i>" % note.level)
        output_file.write(self.html(note.text))
        output_file.write("</h%i>" % note.level)
    
    def write_comment(self, comment, output_file):
    
        output_file.write("<p>")
        output_file.write(self.html(comment.text))
        output_file.write("</p>")
    
    def write_equation(self, equation, output_file):
    
        output_file.write("<p>")
        output_file.write(equation.html())
        output_file.write("</p>")
    
    def write_reference(self, reference, output_file):
    
        output_file.write('<a href="#%s">%s</a><br />' % (
            reference.target, self.html(reference.text)))
    
    def write_references(self, references, output_file):
    
        output = []
        
        for ref in references.targets:
            output.append('<a href="#%s">%s</a>' % (ref, self.html(ref)))
        
        output_file.write(references.text + ", ".join(output))
    
    def write_target(self, target, output_file):
    
        output_file.write('<a name="%s" />' % target.name)


class Note:

    def __init__(self, text, level = 2):
    
        self.text = text
        self.level = level


class HTMLReference:

    def __init__(self, text, target):
    
        self.text = text
        self.target = target


class HTMLReferences:

    def __init__(self, text, targets):
    
        self.text = text
        self.targets = targets


class Target:

    def __init__(self, name):
    
        self.name = name


class Comment:

    def __init__(self, text):
    
        self.text = text

# Objects and the document

class Object:
    
    def read_word(self, text):
    
        return struct.unpack("<I", text[:4])[0]

    def read_half_word(self, text):
    
        return struct.unpack("<H", text[:4])[0]

class StyleType:

    def __init__(self, name, value):
    
        self.name = name
        self.type = value
    
    def __repr__(self):
    
        return "<StyleType: %s (%i)>" % (self.name, self.type)

class StyleInfo:

    Document = StyleType("Document", 0)
    Chapter = StyleType("Chapter", 1)
    Section = StyleType("Section", 2)
    List = StyleType("List", 3)
    Figure = StyleType("Figure", 4)
    Table = StyleType("Table", 5)
    Maths = StyleType("Maths", 6)
    Picture = StyleType("Picture", 7)
    Text = StyleType("Text", 8)
    Note_Area = StyleType("Note_Area", 23)
    Note = StyleType("Note", 24)
    Matrix = StyleType("Matrix", 25)
    Unknown = StyleType("Unknown", -1)
    
    types = {
        0: Document,
        1: Chapter,
        2: Section,
        3: List,
        4: Figure,
        5: Table,
        6: Maths,
        7: Picture,
        8: Text,
        23: Note_Area,
        24: Note,
        25: Matrix
        }

class Style(StyleInfo):

    def __init__(self, name, length, structure_type, location, reference, others = ()):
    
        self.name = name
        self.pair = (length, structure_type)
        self.length = length
        self.type = self.types.get(structure_type, self.Unknown)
        self.location = location
        self.definition = reference
        self.others = others

unknown_style = Style("[unknown]", -1, -1, -1, ())

class Picture(Object):

    def __init__(self, document, style, length, metadata, location):
    
        self.document = document
        self.style = style
        self.length = length
        self.metadata = metadata
        self.location = location
    
    def picture_data(self):
    
        try:
            # The length of the data (not stored since it appears to be
            # padding) is actually the index into the table of the area
            # containing all the picture data.
            offset = length
            return self.document.structures["pictures"][offset]
        
        except IndexError:
            return None

class Text(Object):

    charmap = {
        0x40: "&middot;",
        0x91: "(-+)",
        0x96: "&infin;",
        0xa7: "&sect;",
        0xac: "&equiv;",
        0xae: "&cong;",
        0xc6: "&le;",
        0xc9: "&int;",
        0xdb: "&nabla;",
        0xe1: "&tilde;",
        0xee: "&rArr;",
        0xfb: "&part;"
        }
    
    def __init__(self, document, style, inline_style_data, text, metadata, location):
    
        self.document = document
        self.style = style
        self.inline_style_data = inline_style_data
        self.text = text
        self.metadata = metadata
        self.location = location
    
    def inline_styles(self):
    
        styles = []
        prefix = []
        current = 0
        text = ""
        for i in range(0, len(self.inline_style_data), 11):
            piece = self.inline_style_data[i:i + 11]
            if len(piece) < 11:
                prefix.append(piece)
                break
            
            offset = self.read_word(piece[0:3]+"\x00")
            length = self.read_word(piece[3:6]+"\x00")
            table = ord(piece[6]), ord(piece[7])
            entry = ord(piece[8]), ord(piece[9]), ord(piece[10])
            if entry[0] != 0 or entry[2] != 0:
                prefix.append(piece)
                continue
            
            styles.append(self.text[current:offset])
            if table[1] == 1:
                styles.append(None)
            elif table[1] == 2:
                styles.append(Reference(self.document, table[1], entry[1], 0).dereference())
            else:
                print "Unknown style type (%x) at offset %x in structure at %x" % (table[1], i + 7, self.location)
            
            styles.append(self.text[offset:offset + length])
            current = offset + length
        
        styles.append(self.text[current:])
        return prefix + styles

class Equation(Text):

    greek = {
        "A": "&#913;", "B": "&#914;", "C": "&#915;", "D": "&#916;",
        "E": "&#917;", "Z": "&#918;", "H": "&#919;", "": "&#920;",
        "I": "&#921;", "K": "&#922;", "L": "&#923;", "M": "&#924;",
        "N": "&#925;", "X": "&#926;", "O": "&#927;", "P": "&#928;",
        "R": "&#929;", "S": "&#931;", "T": "&#932;", "U": "&#933;",
        "V": "&#934;", "F": "&#935;", "Y": "&#936;", "W": "&#937;",
        "a": "&#945;", "b": "&#946;", "c": "&#947;", "d": "&#948;",
        "e": "&#949;", "z": "&#950;", "g": "&#951;", "h": "&#952;",
        "i": "&#953;", "k": "&#954;", "l": "&#955;", "m": "&#956;",
        "n": "&#957;", "x": "&#958;", "o": "&#959;", "p": "&#960;",
        "r": "&#961;", "": "&#962;", "s": "&#963;", "t": "&#964;",
        "u": "&#965;", "f": "&#966;", "v": "&#967;", "y": "&#968;",
        "w": "&#969;"
    }
    
    mathphys_greek = {
        "A": "&Alpha;", "B": "&Beta;", "X": "&Chi;", "D": "&Delta;",
        "E": "&Epsilon;", "F": "&Phi;", "G": "&Gamma;", "H": "&Eta;",
        "I": "&Iota;", "J": "&phi;",  "K": "&Kappa;", "L": "&Lambda;",
        "M": "&Mu;",   "N": "&Nu;",   "O": "&Omicron;", "P": "&Pi;",
        "Q": "&Theta;", "R": "&Rho;", "S": "&Sigma;", "T": "&Tau;",
        "U": "&Upsilon;", "V": "&#935;", "W": "&Omega;", "X": "&Xi;",
        "Y": "&Psi;", "Z": "&Zeta;",
        "a": "&alpha;", "b": "&beta;", "c": "&chi;", "d": "&delta;",
        "e": "&epsilon;", "f": "&phi;", "g": "&gamma;", "h": "&eta;",
        "i": "&iota;", "j": "&phi;", "k": "&kappa;", "l": "&lambda;",
        "m": "&mu;",   "n": "&nu;",   "o": "&omicron;", "p": "&pi;",
        "q": "&theta;", "r": "&rho;", "s": "&sigma;", "t": "&tau;",
        "u": "&upsilon;", "v": "&#935;", "w": "&omega;", "x": "&xi;",
        "y": "&psi;", "z": "&zeta;"
    }
    
    command = {
        0x06: "division", 0x09: "inline text",
        0x26: "equation alignment",
        0x87: "subscript", 0x47: "superscript",
        0xc1: "sqrt",
        0xc7: "super and subscript"
        }
    
    def __init__(self, document, style, text, metadata, location):
    
        self.document = document
        self.style = style
        self.text = text
        self.metadata = metadata
        self.location = location
    
    def html(self):
    
        self.current_font = ord(self.text[1])
        self.commands = []
        
        i = 3
        #print "->"
        i, text = self._html(i)
        #print text
        return "".join(map(self.decode, text))
    
    def _html(self, i, remaining = -1):
    
        text = []
        while i < len(self.text) and remaining != 0:
        
            char_type = ord(self.text[i])
            char = self.text[i + 1]
            count = ord(self.text[i + 2])
            #print i, remaining, hex(char_type), repr(char)
            
            if char_type & 0xb == 0xb and char != "\x00":
                # Not sure about the font/count relationship.
                if count >= 5:
                    self.current_font = count
                
                if self.current_font == 5:
                    text.append(char)
                elif self.current_font == 6:
                    text.append(Italic(char))
                elif self.current_font == 7:
                    text.append(self.mathphys_greek.get(char, char))
                elif self.current_font == 8:
                    text.append(char)
                elif self.current_font == 9: # May be able to do something with bit combinations.
                    text.append(Bold(char))
                else:
                    text.append(char)
                i += 4
            
            elif count == 0:
                # A command with following arguments.
                if char_type & 0x20:
                    # Equation alignment flag is set.
                    char_type = char_type & ~0x20
                
                if char_type == 0xc1:
                    # Square root
                    self.commands.append(self.command[char_type])
                    i, (a1,) = self._html(i + 4, 1)
                    text.append("&radic;["+a1+"]")
                
                elif char_type == 0x06:
                    # Division
                    self.commands.append(self.command[char_type])
                    i, (a1, a2) = self._html(i + 4, 2)
                    text.append("("+a1+"/"+a2+")")
                
                elif char_type == 0x09:
                    # Inline text
                    self.commands.append(self.command[char_type])
                    # The character contains an index into what appears to be
                    # the first block in the reference area or, alternatively,
                    # an index into the first area in the document.
                    # (It may not always be the first of either, of course.)
                    reference = Reference(self.document, ord(self.text[i+3]),
                                          ord(char), 0)
                    print reference.table, reference.table_position
                    text.append(self.markup(reference.dereference().text))
                    i += 4
                
                # Note: For subscript and superscript, it may be that, if
                # the first argument (corresponding to the symbol to which
                # the script is attached) has a count of 2 or greater, the
                # second character in the argument is actually placed above
                # or below the symbol.
                
                elif char_type == 0x87:
                    # Subscript
                    self.commands.append(self.command[char_type])
                    i, (a1, a2) = self._html(i + 4, 2)
                    text.append(a1+"<sub>"+a2+"</sub>")
                
                elif char_type == 0x47:
                    # Superscript
                    self.commands.append(self.command[char_type])
                    i, (a1, a2) = self._html(i + 4, 2)
                    text.append(a1+"<sup>"+a2+"</sup>")
                
                elif char_type == 0xc7:
                    # Superscript and subscript (perhaps could be handled by
                    # checking for the flags used in the previous two cases)
                    self.commands.append(self.command[char_type])
                    i, (a1, a2, a3) = self._html(i + 4, 3)
                    text.append(a1+"<sup>"+a2+"</sup>"+"<sub>"+a3+"</sub>")
                
                else:
                    # Unsupported command
                    print "Command %x unknown at %x (offset %x)" % (char_type, self.location, i)
                    self.commands.append(None)
                    i, args = self._html(i + 4, 2)
                    text += args
                    i += 4
                
                command = self.commands.pop()
            
            else:
                # An argument or argumentless command
                i, new_text = self._html(i + 4, count)
                #if char_type == 0x0e:
                #    text.append("".join(map(lambda t: t.text, new_text)))
                #else:
                if True:
                    text.append("".join(map(str, new_text)))
                # The index will have been updated by this call.
            
            if remaining > 0:
                remaining -= 1
        
        return i, text
    
    def decode(self, text):
    
        new_text = ""
        for c in str(text):
            new_text += self.charmap.get(ord(c), c)
        return new_text
    
    def markup(self, text):
    
        return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
    

class Italic:

    def __init__(self, text):
    
        self.text = text
    
    def __str__(self):
    
        return "<i>"+self.text+"</i>"

class Bold:

    def __init__(self, text):
    
        self.text = text
    
    def __str__(self):
    
        return "<b>"+self.text+"</b>"

class StyleData(Object, StyleInfo):

    def __init__(self, name, identifier, text, metadata, location):
    
        self.name = name
        self.type = identifier
        self.text = text
        self.metadata = metadata
        self.location = location

class Structure(Object):

    def __init__(self, style, metadata, location):
    
        self.style = style
        self.metadata = metadata
        self.location = location
        self.objects = []
    
    def add_object(self, obj):
    
        self.objects.append(obj)
    
    def __iter__(self):
    
        return StructureIterator(self, 0)
    
    def __next__(self, position):
    
        """__next__(self, position)
        
        Special method specific to Structure subclasses so that the same
        iterator can be used with all of them, but extended by individual
        classes as required.
        """
        try:
            return self.objects[position], position + 1
        except IndexError:
            raise StopIteration
    
    def __replace__(self, position, item):
    
        """__replace__(self, position, item)
        
        Special method specific to Structure subclasses to allow the same
        iterator to be used to replace items in a structure.
        """
        self.objects[position] = item

class StructureIterator:

    def __init__(self, structure, position):
    
        self.structure = structure
        self.position = position
        self.previous = None
    
    def next(self):
    
        self.previous = self.position
        item, self.position = self.structure.__next__(self.position)
        return item
    
    def replace(self, item):
    
        self.structure.__replace__(self.previous, item)

class Matrix(Structure):
    
    def __init__(self, document, style, inline_style_data, text, metadata, location):
    
        self.document = document
        self.style = style
        self.inline_style_data = inline_style_data
        self.text = text
        self.metadata = metadata
        self.location = location
        self.read_data()
    
    def read_data(self):
    
        # The number of rows is found at offset 0x17 and the total number of
        # items is at offset 0x1b. We can also obtain the number of rows from
        # the metadata.
        self.rows = self.read_half_word(self.metadata[:2])
        total = ord(self.inline_style_data[0x1b])
        self.columns = divmod(total, self.rows)[0]
        
        self.objects = []
        row = []
        # Start at offset 0x1f and decode the references that occur every 32
        # bytes.
        for i in range(31, len(self.inline_style_data), 32):
            table = self.read_half_word(self.inline_style_data[i:i+2])
            table_position = self.read_half_word(self.inline_style_data[i+2:i+4])
            unknown = self.read_half_word(self.inline_style_data[i+4:i+6])
            reference = Reference(self.document, table, table_position, unknown)
            row.append(reference)
            if len(row) == self.columns:
                self.objects.append(row)
                row = []
    
    def __iter__(self):
    
        return StructureIterator(self, (0, 0))
    
    def __next__(self, position):
    
        try:
            row, column = position
            items = self.objects[row]
            item = items[column]
            column += 1
            if column == self.columns:
                column = 0
                row += 1
            return item, (row, column)
        except IndexError:
            raise StopIteration
    
    def __replace__(self, position, item):
    
        row, column = position
        self.objects[row][column] = item

class Table(Matrix):

    pass

class Reference:

    def __init__(self, document, table, table_position, unknown):
    
        self.document = document
        self.table = table
        self.table_position = table_position
        self.unknown = unknown
    
    def dereference(self):
    
        return self.document.dereference(self.table, self.table_position, self.unknown)
    
    def __repr__(self):
    
        try:
            text = repr(self.dereference())
            return text[:1] + "Reference to " + text[1:]
        except (KeyError, IndexError):
            return "<Reference %i:%i:%i>" % (self.table, self.table_position, self.unknown)

class PictureData(Object):

    def __init__(self, data, location):
    
        self.data = data
        self.location = location

class DrawFile(PictureData):

    pass

# Internal document objects

class Area:

    """Area
    
    An area containing a lookup table and some definitions or structures.
    """
    def __init__(self, document, start, finish, contains_structures):
    
        self.document = document
        self.start = start
        self.finish = finish
        self.contains_structures = contains_structures
        self.table = []
    
    def read_contents(self):
    
        for entry in range(len(self.table)):
            self.read_entry(entry)
    
    def read_entry(self, entry):
    
        obj = self.table[entry]
        
        if isinstance(obj, InvalidPair):
            return obj
        elif isinstance(obj, ValidPair):
            pair = obj
        else:
            return obj
        
        document = self.document
        document.input_file.seek(pair.start, 0)
        
        obj = document.read_object(
            pair.start, pair.offset, pair.finish, self.contains_structures
            )
        
        if isinstance(obj, Structure):
            document.elements.append(obj)
        elif isinstance(obj, PictureData):
            document.structures["pictures"][entry] = obj
        else:
            document.definitions.append(obj)
        
        self.table[entry] = obj
        document.structures["structures"][pair.start + pair.offset] = obj
        
        return self.table[entry]


class Pair:

    """Pair
    
    An address-offset pair used in lookup tables for areas.
    """
    def __init__(self, address, offset):
    
        self.start = address
        self.offset = offset
        # The finish address should be modified afterwards to refer to
        # either the next valid pair or the end of the area containing the
        # pair.
        self.finish = address

class ValidPair(Pair):

    pass

class InvalidPair(Pair):

    pass


class Document:

    def __init__(self, input_path, output_path, with_bytes = False, store_data = False):
    
        self.input_path = input_path
        self.input_file = open(input_path, "rb")
        self.output_path = output_path
        self.output_file = open(output_path, "w")
        self.input_file.seek(0, 2)
        self.length = self.input_file.tell()
        self.hex_digits = len("%x" % self.length)
        self.input_file.seek(0, 0)
        self.structures = {}
        self.data = {}
        self.objects = {}
        self.writer = HTMLWriter(self.hex_digits)
        self.writer.with_bytes = with_bytes
        self.store_data = store_data
    
    def dereference(self, table, table_position, unknown = 0):
    
        start, discard = self.structures["main table"][table]
        area = self.structures["areas"][start]
        obj = area.read_entry(table_position)
        
        return obj
    
    def read_word(self, text = None):
    
        if not text:
            text = self.input_file.read(4)
            return struct.unpack("<I", text)[0], text
        else:
            return struct.unpack("<I", text[:4])[0]
    
    def read(self):
    
        self.read_main_table()
        addresses = self.structures["main table"]
        addresses.sort()
        
        # Read the part before the first area.
        start, discard = addresses[0]
        self.structures["styles"] = self.read_styles_list(start)
        self.styles = self.structures["styles"].values()
        
        # Append the length of the file to the list of area addresses to allow
        # us to read to the end of the file.
        #addresses.append(self.length)
        self.structures["areas"] = {}
        self.structures["structures"] = {}
        self.structures["pictures"] = {}
        self.elements = []
        self.definitions = []
        
        for i in range(len(addresses)-1):
            start, structure = addresses[i]
            finish, discard = addresses[i+1]
            area = self.read_area(start, finish, structure)
            self.structures["areas"][start] = area
        
        # Ensure that all the styles are dereferenced.
        for style in self.styles:
            style.definition = style.definition.dereference()
        
        # Read the contents of all the areas.
        for area in self.structures["areas"].values():
            area.read_contents()
        
        # Examine the last area in the file separately.
        start, discard = addresses[-1]
        area = self.read_reference_area(start, self.length)
        self.structures["areas"][start] = area
    
    def read_main_table(self):
    
        addresses = []
        valid1 = (0x20, 0x24, 0x28, 0x2c)
        valid2 = (0x30, 0x34, 0x38, 0x3c,
                  0x40, 0x44, 0x48, 0x58, 0xc0)
        
        for address in valid1 + valid2:
        
            self.input_file.seek(address, 0)
            pos = self.input_file.tell()
            
            word, text = self.read_word()
            if word != 0 and word <= self.length:
                addresses.append((word, address in valid2))
        
        self.structures["main table"] = addresses
    
    def read_area(self, start, finish, contains_structures = False):
    
        if self.store_data:
            self.data[(start, start)] = Note("Area")
        
        area = Area(self, start, finish, contains_structures)
        
        #print "In area", hex(start), hex(finish)
        
        # Store the table entries so that the contents of the area can be
        # examined when all areas have been scanned.
        area.table = self.read_table(start, finish)
        return area
    
    def read_table(self, start, finish):
    
        # Begin at the start of the area.
        self.input_file.seek(start, 0)
        
        data = ""
        
        # Read the initial length word.
        length, bytes = self.read_word()
        data += bytes
        
        if length % 4 != 0:
        
            data += self.input_file.read(finish - start - 4)
            if self.store_data:
                self.data[(start, finish)] = data
            return []
        
        pairs = []
        previous_valid = None
        for i in range(0, length, 8):
        
            address, bytes = self.read_word()
            data += bytes
            offset, bytes = self.read_word()
            data += bytes
            if address <= finish:
            
                # Store the valid pair in the list.
                pair = ValidPair(address, offset)
                pairs.append(pair)
                
                # If there was a previous pair in the list, add the address of
                # this pair to it so that we know the extent of the structure
                # it represents.
                if previous_valid:
                    previous_valid.finish = address
                
                # This pair is now the previous valid pair.
                previous_valid = pair
            else:
                # Store the invalid pair in the list.
                pairs.append(InvalidPair(address, offset))
        
        # The final valid pair must be given the finishing address of the
        # area, so that the extent of the final structure in the area is
        # fully defined.
        if previous_valid:
            previous_valid.finish = finish
        
        if self.store_data:
            self.data[(start, start + length + 4)] = data
        #print "table", hex(start), hex(start + length)
        return pairs
    
    def read_object(self, start, offset, finish, structure):
    
        if structure:
            obj = self.read_structure(start, offset, finish)
        else:
            obj = self.read_definition(start, offset, finish)
        
        return obj
        
    def read_definition(self, start, offset, finish):
    
        # Create targets for this definition in the HTML output.
        if self.store_data:
            self.data[(start, 0)] = Target(("%%0%ix" % self.hex_digits) % start)
            self.data[(start + offset, start + offset + 1)] = Target(("%%0%ix" % self.hex_digits) % (start + offset))
        
        self.input_file.seek(start, 0)
        data = self.input_file.read(11)
        
        # The first word is different for different kinds of definitions,
        # though this may just be a coincidence.
        
        word = self.read_word(data[:4])
        identifier = word >> 16
        
        if identifier in self.structures["styles"]:
        
            # Text
            style = self.structures["styles"][identifier]
        
        else:
        
            style = self.dereference(identifier & 0xff, identifier >> 8, 0)
        
        if True:
        
            if isinstance(style, Style):
                name, others = style.name, style.others
                if self.store_data:
                    self.data[(start, start)] = Note("Definition (using %s)" % name, 3)
            elif self.store_data:
                self.data[(start, start)] = Note("Definition (using %s)" % style, 3)
            
            if self.store_data:
                self.data[(start, start + 11)] = data
            
            length = self.input_file.read(3)
            content_id = self.input_file.read(2)
            
            if self.store_data:
                self.data[(start + 11, start + 11)] = Note("Length", 4)
                self.data[(start + 11, start + 14)] = length
                self.data[(start + 14, start + 14)] = Note("Identifier", 4)
                self.data[(start + 14, start + 16)] = content_id
            
            length = self.read_word(length + "\x00")
            content_id = self.read_word(content_id + "\x00\x00")
            
            data_start = 16
            
            # Read the data between the start of the data and the offset.
            # If the length is less than the total data available, it means
            # that the text is preceded by some information about style
            # changes within the body of the text.
            
            inline_length = max(0, offset - data_start - length)
            if inline_length > 0:
            
                inline_style_data = self.input_file.read(inline_length)
                if self.store_data:
                    self.data[(start + data_start, 0)] = Note("Inline Style Information", 4)
                    self.data[(start + data_start, start + data_start + inline_length)] = inline_style_data
                
                # For some inline style information, the data contains fields of bytes
                # of the form 3-3-5, where the first three bytes correspond to an
                # offset* in the text (also appearing in the metadata that follows the
                # text), the length of the text effect, and a reference to the effect
                # information.
                # The effect may be an inline expression, in which case
                # the 5 bytes are broken up into a 1-2-2 sequence where the first
                # pair of bytes refer to the area in the main table (or a block in the
                # reference area) and the second pair refers to an entry in the area
                # (or block's) table. The address found this way is that of an equation
                # structure.
                # Pure text effects appear to involve 1-1-3 sequences where the first
                # two bytes are both 01, and the 3 byte sequence contains a value
                # surrounded by two zero bytes. In some documents, italic is 12 and
                # bold is 13. Although one draft chapter (Petschek) uses 14, the full
                # thesis document (Final2) uses 17 at the same place, and the
                # PostScript file shows plain text in one place, and italic in the
                # caption for a table.
                #
                # * Such offsets refer to offsets in the text where a style change or
                # line break occurs.
                
            
            else:
                inline_style_data = ""
            
            if content_id == 0x8100:
                if self.store_data:
                    self.data[(start + data_start + inline_length, 0)] = Note("Equation", 4)
            elif content_id == length << 8:
                if self.store_data:
                    self.data[(start + data_start + inline_length, 0)] = Note("Picture", 4)
            elif self.store_data:
                self.data[(start + data_start + inline_length, 0)] = Note("Text", 4)
            
            text = self.input_file.read(offset - data_start - inline_length)
            if self.store_data:
                self.data[(start + data_start + inline_length, start + offset)] = text
            #print repr(data)
            
            # Read the metadata after the offset.
            metadata = self.input_file.read(finish - start - offset)
            if self.store_data:
                self.data[(start + offset, finish)] = metadata
            
            if metadata:
                # The first half word appears to be the number of pieces (not
                # necessarily just lines) used to display the text.
                lines = self.read_word(metadata[:2] + "\x00\x00")
            
            # From examining an internal data dump for an unrelated document,
            # it seems that the type of structure is defined in the associated
            # style. This means we can avoid having to guess about the
            # type using the content ID which, according to a brief comparison
            # between the data dump and my own documents, is apparently the
            # language in use.
            
            if style.type == style.Maths:
                definition = Equation(self, style, text, metadata, start)
                if self.store_data:
                    self.data[(finish - 2, finish)] = Note("Translation", 4)
                    self.data[(finish - 1, finish)] = definition
            elif style.type == style.Picture:
                definition = Picture(self, style, length, metadata, start)
            elif style.type == style.Matrix:
                definition = Matrix(self, style, inline_style_data, text, metadata, start)
            elif style.type == style.Table:
                definition = Table(self, style, inline_style_data, text, metadata, start)
            else:
                definition = Text(self, style, inline_style_data, text, metadata, start)
        
        else: # We should never encounter style definitions where content
              # definitions are expected.
        
            # Style definition
            
            length = self.input_file.read(1)
            if self.store_data:
                self.data[(start, start + 11)] = data
                self.data[(start + 11, start + 11)] = Note("Length", 4)
                self.data[(start + 11, start + 12)] = length
            
            length = ord(length)
            
            definition = self.read_style_definition(start, length, offset, finish)
        
        # Store the location of the object in the objects dictionary.
        self.objects[start + offset] = definition
        definition.extent = finish - start - offset
        
        #else:
        #    self.data[(start, start)] = Note("Definition (%x)" % start, 3)
        #    data += self.input_file.read(offset - 11)
        #    self.data[(start, start + offset)] = data
        #    self.data[(start + offset, finish)] = self.input_file.read(finish - start - offset)
        #    return
        
        #if offset - data_start != length:
        #    print "Offset (%x) does not refer to point after data beginning at %x" % (start + offset, start)
        #print hex(start), hex(start + offset)
        
        #xpos = self.read_word(data[4:6])
        #ypos = self.read_word(data[6:8])
        #print hex(start + offset), hex(address2)
        
        return definition
    
    def read_style_definition(self, start, length, offset, finish):
    
        identifier = self.input_file.read(1)
        if self.store_data:
            self.data[(start + 12, start + 12)] = Note("Identifier", 4)
            self.data[(start + 12, start + 13)] = identifier
        identifier = ord(identifier)
        data_start = 13
        
        if self.store_data:
            self.data[(start + data_start, start + data_start)] = Note("Text", 4)
        
        # Read the data between the start of the data and the offset.
        text = self.input_file.read(offset - data_start)
        if self.store_data:
            self.data[(start + data_start, start + offset)] = text
        
        name = text[:length]
        
        for style in self.structures["styles"].values():
            if style.pair == (length, identifier) and name == style.name:
                # The definition contains information about this style.
                if self.store_data:
                    self.data[(start, start)] = Note("Style Definition for %s" % style.name, 3)
                break
        else:
            if self.store_data:
                self.data[(start, start)] = Note("Style Definition (%x)" % start, 3)
        
        # Read the metadata after the offset.
        metadata = self.input_file.read(finish - start - offset)
        
        if finish - start - offset > 0:
            if self.store_data:
                self.data[(start + offset, finish)] = metadata
        
        # Looking at the definitions with the same identifiers and examining
        # an internal data dump for an unrelated document, it appears that
        # the identifier is actually the type of the structure defined by
        # each definition.
        
        definition = StyleData(name, identifier, text, metadata, start)
        # The third byte looks like a marker that also appears after the
        # font string.
        #marker = data[2]
        
        # Start at the next word boundary.
        #i = 4 - ((start + offset) % 4)
        
        # The first two bytes are the length of a font string, if present.
        #length = self.read_word(data[i:i+2] + "\x00\x00")
        
        ###
        return definition
    
    def read_structure(self, start, offset, finish):
    
        # Create a target to this definition in the HTML output.
        if self.store_data:
            self.data[(start, 0)] = Target(("%%0%ix" % self.hex_digits) % start)
            self.data[(start + offset, start + offset + 1)] = Target(("%%0%ix" % self.hex_digits) % (start + offset))
        
        self.input_file.seek(start, 0)
        data = self.input_file.read(12)
        
        # The first word may refer to an existing structure style, indicating
        # that the data is a document structure (chapter, section, figure, etc.)
        # that is formatted using that style.
        
        word = self.read_word(data[:4])
        
        # The first two bytes correspond to the number of elements held by a
        # structure; the second two are used to identify the structure style
        # in use.
        identifier = word >> 16
        
        if identifier in self.structures["styles"]:
        
            # Structure
            structure_type = ord(data[11])
            
            # Read the rest of the first sixteen bytes.
            data += self.input_file.read(4)
            
            style = self.structures["styles"][identifier]
            name, others = style.name, style.others
            if self.store_data:
                self.data[(start, start)] = Note("Definition (using %s)" % name, 3)
                self.data[(start, start + 16)] = data
            
            data_start = 16
            
            # Read the data between the start of the data and the offset.
            # The first part of the data is a series of references to
            # information via areas stored in the main table.
            
            inline_length = max(0, offset - data_start)
            if inline_length > 0:
            
                inline_data = self.input_file.read(inline_length)
                if self.store_data:
                    self.data[(start + data_start, 0)] = Note("Structure References", 4)
                    self.data[(start + data_start, start + data_start + inline_length)] = inline_data
            
            # Read the metadata after the offset.
            metadata = self.input_file.read(finish - start - offset)
            if self.store_data:
                self.data[(start + offset, finish)] = metadata
            
            if metadata:
                definition = Structure(style, metadata, start)
                
                # The first half word appears to be the number of structures.
                pieces = self.read_word(metadata[:2] + "\x00\x00")
                if self.store_data:
                    self.data[(start + offset, start + offset + 2)] = Comment("%i pieces" % pieces)
                
                # Work backwards from the end of the inline data to retrieve references to
                # structures. Each three bytes (possibly three pairs of bytes) refers to a
                # structure: the first is the number of an entry in the main table which is
                # used to obtain the address of an area, the second is the number of an
                # entry in the area's table, and the third has an unknown purpose.
                
                objects = []
                i = len(inline_data)
                while pieces > 0:
                    i -= 2
                    unknown = self.read_word(inline_data[i:i+2] + "\x00\x00")
                    i -= 2
                    table_position = self.read_word(inline_data[i:i+2] + "\x00\x00")
                    i -= 2
                    table = self.read_word(inline_data[i:i+2] + "\x00\x00")
                    objects.append(self.dereference(table, table_position, unknown))
                    pieces -= 1
                    if self.store_data:
                        self.data[(start + data_start + i, 0)] = Reference(
                            self, table, table_position, unknown
                            )
                
                objects.reverse()
                for obj in objects:
                    definition.add_object(obj)
        
        elif data[8:12] == "Draw":
        
            # Draw file
            definition = DrawFile(data[8:12] + self.input_file.read(finish - start - 12), start)
            if self.store_data:
                self.data[(start, start)] = Note("Drawfile (%x)" % start, 3)
                self.data[(start + 12, finish)] = Comment("%i bytes not shown" % (finish - start - 8))
        
        else:
        
            # Style definition
            
            length = data[11]
            if self.store_data:
                self.data[(start, start + 11)] = data[:11]
                self.data[(start + 11, start + 11)] = Note("Length", 4)
                self.data[(start + 11, start + 12)] = length
            
            length = ord(length)
            
            definition = self.read_style_definition(start, length, offset, finish)
        
        # Store the location of the object in the objects dictionary.
        self.objects[start + offset] = definition
        definition.extent = finish - start - offset
        
        return definition

    def read_reference_area(self, start, finish):
    
        self.input_file.seek(start, 0)
        initial_data = self.input_file.read(0x88)
        
        if self.store_data:
            self.data[(start, start)] = Note("Reference Area")
            self.data[(start, start + 0x88)] = initial_data
        
        # Assume that there is a table 0x88 bytes into this area.
        data = ""
        first, bytes = self.read_word()
        data += bytes
        addresses = [first]
        previous_valid = False
        document_block = None
        
        while self.input_file.tell() < first:
        
            word, bytes = self.read_word()
            data += bytes
            if start <= word <= finish:
                addresses.append(word)
                previous_valid = True
            else:
                # The fifth address appears to refer to the block whose first
                # entry itself refers to the document structure definition.
                # It appears to be followed by a zero word, so we'll use that
                # to mark any preceding valid address as the block containing
                # this reference.
                if word == 0 and previous_valid:
                    document_block = addresses[-1]
                previous_valid = False
        
        area = Area(self, start, finish, True)
        area.table = addresses
        area.references = []
        
        if self.store_data:
            self.data[(start + 0x88, start + 0x88)] = Note("Table", 3)
            self.data[(start + 0x88, first)] = data
        
        # The data at each address referred to in the table itself begins with
        # a length word and is followed by address-length pairs, where the
        # length for each address is the length of the data it refers to.
        
        # We could use the length word to check that the addresses in this
        # table are consistent with the data they refer to but, for now, we'll
        # just assume that they're all valid.
        
        # The last address in the list should be the extent of the file.
        
        for address in addresses[:-1]:
        
            self.input_file.seek(address, 0)
            data = ""
            length, bytes = self.read_word()
            data += bytes
            
            found = []
            missing = []
            
            for i in range(0, length, 8):
            
                location, bytes = self.read_word()
                data += bytes
                data_length, bytes = self.read_word()
                data += bytes
                if data_length == 0:
                    continue
                
                if location in self.objects and data_length == self.objects[location].extent:
                    found.append(location)
                else:
                    missing.append(location)
            
            position = self.input_file.tell()
            if self.store_data:
                self.data[(address, address)] = Note("Block", 3)
                self.data[(address, position)] = data
            if found:
                if self.store_data:
                    self.data[(position-1, position)] = HTMLReferences("Refs: ", map(lambda x: ("%%0%ix" % self.hex_digits) % x, found))
                area.references.append(found)
            if missing:
                if self.store_data:
                    self.data[(position-1, position)] = HTMLReferences("Missing: ", map(lambda x: ("%%0%ix" % self.hex_digits) % x, found))
            
            if address == document_block and len(found) != 0:
                self.document_structure = self.structures["structures"][found[0]]
        
        area.read_contents()
        return area
    
    def read_styles_list(self, top):
    
        self.input_file.seek(0, 0)
        data = self.input_file.read(top)
        
        position = top - 1
        styles = {}
        number = 0
        name = ""
        while position > 0:
        
            if data[position] != "\x00":
                name = data[position] + name
                position -= 1
                continue
            
            # The current byte is a zero byte. It is preceded by two
            # identifier bytes.
            identifier = self.read_word(data[position-2:position] + "\x00\x00")
            
            # The identifier can be used to access the style definition via
            # the main table in the same way as document structure definitions.
            reference = Reference(self, identifier & 0xff, identifier >> 8, 0)
            
            # Before these are three unknown bytes.
            unknown1 = data[position-4:position-2]
            # Before this is another unknown byte, usually found with the
            # string length byte.
            # According to information in an internal data dump, this is the
            # type of structure that the style definition describes.
            structure_type = ord(data[position-5:position-4])
            # Before this is a byte containing the length of the string.
            length = ord(data[position-6:position-5])
            styles[identifier] = Style(name, length, structure_type, position - 6, reference, others = (unknown1,))
            
            if self.store_data:
                self.data[(position, position)] = Comment(
                    "%s (identifier = %04x; unknown = %04x, length = %i, type = %i)" % (
                        name, identifier, self.read_word(unknown1 + "\x00\x00"),
                        length, structure_type
                    ))
            
            name = ""
            
            # Move to the end of the previous definition, or to the start of
            # the table.
            position -= 7
            
            if data[position] == "\x00":
            
                # The beginning of the table (assuming that only 255 styles
                # can be defined).
                number = ord(data[position-1:position])
                break
        
        if self.store_data:
            self.data[(0, top)] = data
            self.data[(position, position)] = Note("Styles")
            self.data[(position + 1, position + 1)] = Comment("%i styles" % number)
        
        return styles
    
    def resolve_references(self, parent = None):
    
        if parent is None:
            try:
                parent = self.document_structure
            except AttributeError:
                return
        
        it = iter(parent)
        try:
            while True:
            
                ref = it.next()
                obj = ref.dereference()
                it.replace(obj)
                
                if isinstance(obj, Structure):
                    self.resolve_references(obj)
        
        except StopIteration:
            pass
    
    def write(self):
    
        self.writer.write_header(self.input_path, self.output_file)
        digits = self.hex_digits
        
        keys = self.data.keys()
        keys = filter(lambda key: type(key) == type(()), keys)
        keys.sort()
        
        for key in keys:
            start, finish = key
            data = self.data[(start, finish)]
            self.writer.write(data, self.output_file, start, finish, digits)
        
        self.writer.write_footer(self.output_file)
    
    def close(self):
    
        self.input_file.close()
        self.output_file.close()
    
    def show_structure(self, indent = 0, parent = None):
    
        if parent is None:
            try:
                parent = self.document_structure
            except AttributeError:
                return
        
        print indent * "  ", parent, parent.style.name
        
        for obj in parent:
        
            if isinstance(obj, Structure):
                self.show_structure(indent + 1, obj)
            else:
                if hasattr(obj, "style"):
                    if isinstance(obj.style, Reference):
                        style = "[unnamed style at %x]" % obj.style.dereference().location
                    else:
                        style = obj.style.name
                else:
                    style = ""
                print indent * "  ", obj, style


if __name__ == "__main__":

    usage = "[--bytes] <input file> <output HTML file>"
    syntax = cmdsyntax.Syntax(usage)
    matches = syntax.get_args(sys.argv[1:])
    
    if len(matches) != 1:
    
        sys.stderr.write("Usage: %s %s\n" % (sys.argv[0], usage))
        sys.exit(1)
    
    input_path = matches[0]["input file"]
    output_path = matches[0]["output HTML file"]
    with_bytes = matches[0].get("bytes", 0) != 0
    
    try:
        document = Document(input_path, output_path, with_bytes, store_data = True)
        document.read()
    except IOError:
        sys.stderr.write("Failed to open file for reading: %s\n" % matches[0]["input file"])
        sys.exit(1)
    
    try:
        document.write()
        document.close()
    except IOError:
        sys.stderr.write("Failed to write file: %s\n" % matches[0]["output HTML file"])
        sys.exit(1)
    
    #sys.exit()
