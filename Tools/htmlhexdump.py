#!/usr/bin/env python

"""
htmlhexdump.py - A document analysis tool for the TechWriter Python package.

Copyright (C) 2011 David Boddie <david@boddie.org.uk>

This file is part of the TechWriter Python package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import cmdsyntax, sys

def html(text):

    return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

def write_line(input_file, output_file, number, format, finish, with_bytes):

    offset = input_file.tell()
    hex_offset = format % offset
    
    bytes = input_file.read(min(number, finish - offset))
    if not bytes:
        return False

    codes = []
    code = ""
    chars = []
    words = []
    word = ""
    word_styles = ["word-even", "word-odd"]
    for byte in bytes:
        if with_bytes:
            code = code + "%02x" % ord(byte) + "&nbsp;"
        word = "%02x" % ord(byte) + word
        if len(word) == 8:
            if with_bytes:
                codes.append('<span class="' + word_styles[0] + '">' + code + '</span>')
                code = ""
            words.append('<span class="' + word_styles[0] + '">' + word + '</span>')
            word_styles.insert(0, word_styles.pop())
            word = ""
        if 32 <= ord(byte) <= 126:
            chars.append(byte)
        else:
            chars.append('<span class="unprintable">?</span>')
    
    if word:
        word = "&nbsp;"*(8 - len(word)) + word
        words.append('<span class="' + word_styles[0] + '">' + word + '</span>')
        word_styles.insert(0, word_styles.pop())
    if with_bytes and code:
        codes.append(code)
    if len(bytes) == number:
        success = True
    else:
        remaining = ((number >> 2) - len(words))
        while remaining > 0:
            words += ['<span class="' + word_styles[0] + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>']
            word_styles.insert(0, word_styles.pop())
            remaining -= 1
        if with_bytes:
            codes += (number - len(bytes)) * ["&nbsp;&nbsp;"]
        chars += (number - len(bytes)) * ["&nbsp;"]
        success = False
    
    if with_bytes:
        output_file.write('<span class="offset">' + hex_offset + '</span>' + ":&nbsp;"
                        + "&nbsp;".join(words) + "&nbsp;:&nbsp;"
                        + "".join(chars) + "&nbsp;:&nbsp;" + "".join(codes) + "\n")
    else:
        output_file.write('<span class="offset">' + hex_offset + '</span>' + ":&nbsp;"
                        + "&nbsp;".join(words) + "&nbsp;:&nbsp;"
                        + "".join(chars) + "\n")
    return success

def write_lines(input_file, output_file, number, start, finish, with_bytes, digits):

    if finish < start:
        input_file.seek(0, 2)
        finish = input_file.tell()
    
    input_file.seek(start, 0)
    
    digits = max(digits, len(str(finish)))
    format = "%%0%dx" % digits
    
    while True:
        if not write_line(input_file, output_file, number, format, finish, with_bytes):
            break

def write_header(path, output_file):

    output_file.write(
        "<html>\n"
        "<head>\n"
        "  <title> Hex dump of " + html(path) + "</title>\n"
        '  <link rel="stylesheet" type="text/css" href="styles.css" />\n'
        "</head>\n"
        "<body>\n"
        "<h1>" + html(path) + "</h1>\n"
        "<pre>\n"
        )

def write_footer(output_file):

    output_file.write(
        "</pre>\n"
        "</body>\n"
        "</html>\n"
        )

if __name__ == "__main__":

    usage = "[-s <start offset>] [-f <finish offset>] [-c] <input file> <output HTML file> [<bytes per line>]"
    syntax_obj = cmdsyntax.Syntax(usage)
    matches = syntax_obj.get_args(sys.argv[1:])
    
    if len(matches) != 1:
        sys.stderr.write("Usage: %s %s\n" % (sys.argv[0], usage))
        sys.exit(1)
    
    match = matches[0]
    try:
        number = int(match.get("bytes per line", 16))
    except ValueError:
        sys.stderr.write("Please specify an integer for the number of bytes per line.\n")
        sys.exit(1)
    
    digits = 0
    try:
        value = match.get("start offset", "0")
        digits = max(digits, len(value))
        start = int(value, 16)
    except ValueError:
        sys.stderr.write("Please specify an integer in hexadecimal form for the start offset.\n")
        sys.exit(1)
    
    try:
        value = match.get("finish offset", "-1")
        digits = max(digits, len(value))
        finish = int(value, 16)
    except ValueError:
        sys.stderr.write("Please specify an integer in hexadecimal form for the finish offset.\n")
        sys.exit(1)
    
    with_bytes = match.get("c", 0) == 1
    
    try:
        input_file = open(match["input file"], "rb")
    except IOError:
        sys.stderr.write("Failed to open file for reading: %s\n" % match["input file"])
        sys.exit(1)
    
    try:
        output_file = open(match["output HTML file"], "w")
        write_header(match["input file"], output_file)
        write_lines(input_file, output_file, number, start, finish, with_bytes, digits)
        write_footer(output_file)
    except IOError:
        sys.stderr.write("Failed to write file: %s\n" % match["output HTML file"])
        sys.exit(1)
    
    input_file.close()
    output_file.close()
    sys.exit()
